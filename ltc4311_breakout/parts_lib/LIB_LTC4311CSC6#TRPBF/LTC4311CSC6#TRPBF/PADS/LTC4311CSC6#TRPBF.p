*PADS-LIBRARY-PART-TYPES-V9*

LTC4311CSC6#TRPBF SOT65P210X100-6N I ANA 7 1 0 0 0
TIMESTAMP 2024.05.05.05.51.34
"Mouser Part Number" 584-LTC4311CSC6TRPBF
"Mouser Price/Stock" https://www.mouser.co.uk/ProductDetail/Analog-Devices/LTC4311CSC6TRPBF?qs=hVkxg5c3xu8dDGkXCTUXoQ%3D%3D
"Manufacturer_Name" Analog Devices
"Manufacturer_Part_Number" LTC4311CSC6#TRPBF
"Description" Interface - Specialized I2C/SMBus Rise Time Accelerator
"Datasheet Link" https://www.analog.com/media/en/technical-documentation/data-sheets/4311fa.pdf
"Geometry.Height" 1mm
GATE 1 6 0
LTC4311CSC6#TRPBF
1 0 U VCC
2 0 U GND_1
3 0 U ENABLE
6 0 U BUS1
5 0 U GND_2
4 0 U BUS2

*END*
*REMARK* SamacSys ECAD Model
1393928/1293658/2.50/6/3/Integrated Circuit
